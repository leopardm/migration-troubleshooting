# Migration Troubleshooting

Make sure to set up your local git repo to point to the new remote. This can accomplished using the following commands:

```bash
# Rename old remotes
git remote rename origin old-origin
git remote add origin <new-url>

```

## Common issues

### No repository found

If you are seeing an error stating **No Repository Found**, you can choose to create an empty repo and push everything up from a local copy of the repo (see *The migrated repo is out of date*) or delete the project and notify your GitLab migration specialist of the missing repository

### I can see my group but I can't find my projects

It's possible the projects may have been archived during the migration process. Select the *Archived Projects* tab to see see if they have been archived. 

If they have been archived accidentally, go to settings -> advanced -> unarchive project

### Branches are missing

If you have an up-to-date copy of the recently migrated repo with the missing branch checked out, run:

```
# assuming origin is your new origin
git push origin <new-branch>
```

### The migrated repo is out of date

Our migration utility does NOT overwrite existing projects. If your project was being mirrored/synced to the new instance as a backup, resync the repo with your newest copy.

If you have your branches all checked out locally, run:

```bash
# Assuming origin is the new instance
git push origin --all
git push origin --tags

```

If you need to checkout all branches from the old remote and then push, run:

```
# Assuming old-origin is the remote pointed to your old instance

for branch in `git branch -a | grep remotes | grep -v HEAD | grep -v master `; do
   git branch --track ${branch#remotes/old-origin/} $branch
done

# Assuming origin is the new instance

git push origin --all
git push origin --tags

```
